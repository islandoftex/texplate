import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.api.java.archives.internal.DefaultManifest
import org.islandoftex.texplate.build.CTANBuilderTask
import org.islandoftex.texplate.build.TDSZipBuilderTask

plugins {
    kotlin("jvm") version "2.0.0"
    application

    id("com.diffplug.spotless") version "6.25.0"
    id("com.diffplug.spotless-changelog") version "3.1.2"
    id("com.github.ben-manes.versions") version "0.51.0"
    id("io.gitlab.arturbosch.detekt") version "1.23.6"
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

repositories {
    mavenCentral()
}

spotlessChangelog {
    changelogFile("CHANGELOG.md")
    setAppendDashSnapshotUnless_dashPrelease(true)
    ifFoundBumpBreaking("breaking change")
    tagPrefix("v")
    commitMessage("Release v{{version}}")
    remote("origin")
    branch("master")
}

group = "org.islandoftex"
description = "A document structure creation tool."
version = project.spotlessChangelog.versionNext

kotlin {
    jvmToolchain(8)
}

val projectManifest: Manifest by extra(DefaultManifest((project as ProjectInternal).fileResolver).apply {
    attributes["Implementation-Title"] = project.name
    attributes["Implementation-Version"] = project.version
    "${group}.texplate".let {
        attributes["Main-Class"] = "${it}.MainKt"
        if (java.sourceCompatibility < JavaVersion.VERSION_1_9) {
            attributes["Automatic-Module-Name"] = it
        }
    }
})


dependencies {
    implementation("com.moandjiezana.toml:toml4j") {
        constraints {
            implementation("com.moandjiezana.toml:toml4j:0.7.2") {
                because("this is the latest version for toml4j")
            }
            implementation("com.google.code.gson:gson:2.8.9") {
                because("toml4j pulled a version affected by CVE-2022-25647")
            }
        }
    }
    implementation("org.apache.velocity:velocity-engine-core:2.3")
    implementation("info.picocli:picocli:4.7.6")
    implementation("org.slf4j:slf4j-simple:2.0.13")
}

application {
    mainClass.set("${group}.texplate.MainKt")
}

tasks {
    withType<DependencyUpdatesTask> {
        rejectVersionIf {
            val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { candidate.version.uppercase() in it }
            val isStable = stableKeyword || "^[0-9,.v-]+$".toRegex().matches(candidate.version)
            isStable.not()
        }

        checkForGradleUpdate = false
    }

    named<Task>("assembleDist").configure {
        dependsOn("shadowJar")
    }

    register<TDSZipBuilderTask>("assembleTDSZip")
    register<CTANBuilderTask>("assembleCTAN") {
        dependsOn(":assembleTDSZip")
    }

    jar {
        manifest.attributes.putAll(projectManifest.attributes)
    }
}

spotless {
    kotlinGradle {
        trimTrailingWhitespace()
        endWithNewline()
    }

    kotlin {
        licenseHeader("// SPDX-License-Identifier: BSD-3-Clause")
        target("src/**/*.kt")
        trimTrailingWhitespace()
        endWithNewline()
    }
}

detekt {
    buildUponDefaultConfig = true
    config.setFrom(files("detekt-config.yml"))
}
