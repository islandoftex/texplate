# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.6] - 2024-08-02

### Fixed

* Application version is now retrieved from the manifest (regression fix).

## [1.0.5] - 2024-08-01

### Changed

* Updated dependencies to close a security vulnerability.
  (see #13)

## [1.0.4] - 2021-07-27

### Fixed

* Resolve outdated dependency with vulnerability.
  See #12.

## [1.0.3] - 2020-08-07

### Added

* New `to-string-list-from-file` handler added. This handler converts the map
  entry value into a `File` reference and attempts to read its contents into a
  list of lines. If the file reference does not exist or is invalid (e.g, not a
  file or with insufficient permissions), an empty list is assigned instead. 

### Changed

* `TeXplate` template path resolution has changed. Use `-t article` to get the
  default article template. If you want to specify a file instead, use
  `-t article.toml` or `-t /my/path/to/file.toml`. Relative paths will be
  resolved against the working directory.
  See #8.
* Updated dependencies.

## [1.0.2] - 2020-02-02

### Fixed

* `TeXplate` now finds its templates even on Windows.
  See #11.

### Changed

* `TeXplate` now finishes its transition to Kotlin. We did not change any
  functionality in the course of this change.
  See !6.
* Templates are now provided as resources from the JAR instead of a separate
  folder on the hard drive.

## [1.0.1] - 2020-01-17

### Changed
* `TeXplate` will now distribute only non-generic template file names. In the
  system's template directory, we search for `texplate-<name>.toml` as well.
  Addresses #6. 

## [1.0.0] - 2020-01-15
### Added

* Base functionality and default templates
* User manual

[Unreleased]: https://gitlab.com/islandoftex/texplate/compare/v1.0.6...master
[1.0.6]: https://gitlab.com/islandoftex/texplate/compare/v1.0.5...v1.0.6
[1.0.5]: https://gitlab.com/islandoftex/texplate/compare/v1.0.4...v1.0.5
[1.0.4]: https://gitlab.com/islandoftex/texplate/compare/v1.0.3...v1.0.4
[1.0.3]: https://gitlab.com/islandoftex/texplate/compare/v1.0.2...v1.0.3
[1.0.2]: https://gitlab.com/islandoftex/texplate/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/islandoftex/texplate/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/islandoftex/texplate/-/tags/v1.0.0
